//
//  ResultRecord.swift
//  NoughtsAndCrosses
//
//  Created by Jeremy Pereira on 09/01/2020.
//  Copyright © 2020 Jeremy Pereira. All rights reserved.
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.

import Statistics
import SQL

struct ResultRecord: Recordable
{
	let noughtsName: String
	let crossesName: String
	let result: Game.MoveResult

	static let dataSetName = "noughtsAndCrossesResult"

	static var columns: [(String, SQLType)] =
	[
		("noughtsName", .varChar(255)),
		("crossesName", .varChar(255)),
		("noughtsWin", .integer),
		("crossesWin", .integer),
		("noughtsIllegal", .integer),
		("crossesIllegal", .integer),
		("draw", .integer)
	]
	var data: [String : SQLValue]
	{
		[
			"noughtsName" : .char(noughtsName),
			"crossesName" : .char(crossesName),
			"noughtsWin" : .integer(result.noughtsWon ? 1 : 0),
			"crossesWin" : .integer(result.crossesWon ? 1 : 0),
			"noughtsIllegal" : .integer(result.noughtsIllegal ? 1 : 0),
			"crossesIllegal" : .integer(result.crossesIllegal ? 1 : 0),
			"draw" : .integer(result.isDraw ? 1 : 0),
		]
	}

}
