//
//  Game.swift
//  NoughtsAndCrosses
//
//  Created by Jeremy Pereira on 02/01/2020.
//  Copyright © 2020 Jeremy Pereira. All rights reserved.
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//
import Toolbox
/// Models a game of noughts and crosses
struct Game
{
	/// The moves that have been made so far in this  game
	let moves: [Grid.Coordinate]
	/// The game's current grid
	let grid: Grid
	/// An value that uniquely identifies the game.
	///
	/// When a new game is created by making a move, the id is preserved. The
	/// idea is to allow players to add contextual information without limiting
	/// their state to playing one game at a time.
	let id: UUID

	init()
	{
		self.init(grid: Grid(), moves: [], id: UUID())
	}

	fileprivate init(grid: Grid, moves: [Grid.Coordinate], id: UUID)
	{
		self.grid = grid
		self.moves = moves
		self.id = id
	}

	func moving(at position: Grid.Coordinate) -> MoveResult
	{
		guard winner == nil
			else { return .illegalMove(position, self) }
		let square = moves.count.isMultiple(of: 2) ? Grid.Square.nought : Grid.Square.cross
		do
		{
			let newGrid = try grid.placing(square: square, at: position)
			let updatedGame = Game(grid: newGrid, moves: self.moves + [position], id: id)
			if let winner = updatedGame.winner
			{
				assert(winner == square)
				return .win(winner, updatedGame)
			}
			else if updatedGame.isDrawn
			{
				return .draw(updatedGame)
			}
			else
			{
				return .carryOn(updatedGame)
			}
		}
		catch
		{
			return .illegalMove(position, self)
		}
	}

	var winner: Grid.Square?
	{
		// The earliest win is on noughts third move
		guard let lastMove = moves.last, moves.count >= 5 else { return nil }

		let row = grid.rows[lastMove.y + 1]
		if row[0] != .empty && row[0] == row[1] && row[0] == row[2]
		{
			return row[0]
		}
		let col = grid.columns[lastMove.x + 1]
		if col[0] != .empty && col[0] == col[1] && col[0] == col[2]
		{
			return col[0]
		}
		if lastMove.x == lastMove.y && grid.mainDiagonal[0] != .empty &&  grid.mainDiagonal[0] == grid.mainDiagonal[1] && grid.mainDiagonal[0] == grid.mainDiagonal[2]
		{
			return grid.mainDiagonal[0]
		}
		if  lastMove.x == -lastMove.y && grid.reverseDiagonal[0] != .empty &&  grid.reverseDiagonal[0] == grid.reverseDiagonal[1] && grid.reverseDiagonal[0] == grid.reverseDiagonal[2]
	    {
		    return grid.reverseDiagonal[0]
	    }
		return nil
	}

	var isDrawn: Bool
	{
		return grid.emptySquares == 0
	}

	var whoseGo: Grid.Square
	{
		return moves.count.isMultiple(of: 2) ? Grid.Square.nought : Grid.Square.cross
	}

	/// All the grids in the game
	var allGrids: [Grid]
	{
		let grids = moves.reversed().reduce(into: [grid])
		{
			(gridsSoFar, position) in
			do
			{
				gridsSoFar.append(try gridsSoFar.last!.reversing(position: position))
			}
			catch
			{
				fatalError("Should not be impossible to reverse a legal move")
			}
		}.reversed()
		return Array(grids)
	}
}

extension Game
{
	enum MoveResult
	{
		case illegalMove(Grid.Coordinate, Game)
		case win(Grid.Square, Game)
		case carryOn(Game)
		case draw(Game)

		var game: Game
		{
			switch self
			{
			case .illegalMove(_, let game):
				return game
			case .win(_, let game):
				return game
			case .carryOn(let game):
				return game
			case .draw(let game):
				return game
			}
		}

		var noughtsWon: Bool
		{
			switch self {
			case .win(let square, _):
				return square == .nought
			default:
				return false
			}
		}

		var crossesWon: Bool
		{
			switch self {
			case .win(let square, _):
				return square == .cross
			default:
				return false
			}
		}

		var isDraw: Bool
		{
			switch self
			{
			case .draw:
				return true
			default:
				return false
			}
		}

		var noughtsIllegal: Bool
		{
			switch self {
			case .illegalMove(_, let game):
				return game.whoseGo == .nought
			default:
				return false
			}
		}

		var crossesIllegal: Bool
		{
			switch self {
			case .illegalMove(_, let game):
				return game.whoseGo == .cross
			default:
				return false
			}
		}
	}
}

extension Game.MoveResult
{

	/// Monad style unit function
	/// - Parameter x: A game to turn into a MoveResult
	/// - Returns: A `carryOn` that wraps `x`
	static func unit(_ x: Game) -> Game.MoveResult
	{
		return .carryOn(x)
	}

	/// Bind function
	///
	/// The function is only aplied if `self` is a `.carryOn`. Otherwise, `self`
	/// is returned unaltered.
	/// - Parameter f: The function to bind the `MoveResult` to
	/// - Returns: A `MoveResult`
	func bind(_ f: (Game) -> Game.MoveResult) -> Game.MoveResult
	{
		switch self
		{
		case .illegalMove, .win, .draw:
			return self
		case .carryOn(let game):
			return f(game)
		}
	}

	/// Bind operator for `Game.MoveResult`
	/// - Parameters:
	///   - lhs: The input `MoveResult`
	///   - rhs: The function to apply.
	/// - Returns: The result of applying the function to the unwarapped game if
	///            `lhs` is `.carryOn`
	static func >>- (lhs: Game.MoveResult, rhs: (Game) -> Game.MoveResult) -> Game.MoveResult
	{
		return lhs.bind(rhs)
	}
}

extension Game: Transformable
{
	static func * (lhs: Transformation, rhs: Game) -> Game
	{
		return Game(grid: lhs * rhs.grid, moves: lhs * rhs.moves, id: rhs.id)
	}
}
