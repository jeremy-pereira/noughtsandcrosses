//
//  Player.swift
//  NoughtsAndCrosses
//
//  Created by Jeremy Pereira on 04/01/2020.
//  Copyright © 2020 Jeremy Pereira. All rights reserved.
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//


/// Models a player of a game of noughts and crosses
protocol Player
{
	/// Choose a move to make in a particular game
	///
	/// Note that the type of move: nought or cross is determined by the
	/// number of moves already played.
	/// - Parameter game: The game to make a move in
	/// - Returns: The position of where the playerr wants to move
	mutating func chooseMove(in game: Game) -> Grid.Coordinate

	/// Allows the player to evaluate the result of a game and update any
	/// strategy.
	/// - Parameter result: The `Game.MoveResult` to evaluate.
	mutating func evaluate(result: Game.MoveResult)
}


