//
//  Tournament.swift
//  NoughtsAndCrosses
//
//  Created by Jeremy Pereira on 23/02/2020.
//  Copyright © 2020 Jeremy Pereira. All rights reserved.
//


/// Models a tournament
///
/// A tournament is over a number of games. Each game is between two players.
/// This type schedules the order of games in a tournament.
///
/// As of now, each player is fixed, so the tournament only schedules  the same
/// game over and over.
struct Tournament
{
	private(set) var gamesLeft: Int
	private var noughts: [String]
	private var crosses: [String]

	init(games: Int, noughts: [String], crosses: [String])
	{
		guard noughts.count > 0 else { fatalError("There must be at least one noughts player") }
		guard crosses.count > 0 else { fatalError("There must be at least one crosses player") }
		self.gamesLeft = games
		self.noughts = noughts
		self.crosses = crosses
	}

	/// Return the players for the next game
	///
	/// - Returns: The players for the next game or `nil` of the tournament is
	///            over.
	mutating func next() -> (noughts: String, crosses: String)?
	{
		guard gamesLeft > 0 else { return nil }
		gamesLeft -= 1
		return (noughts: noughts.randomElement()!, crosses: crosses.randomElement()!)
	}
}

