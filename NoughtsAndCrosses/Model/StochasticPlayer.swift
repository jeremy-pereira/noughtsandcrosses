//
//  StochasticPlayer.swift
//  NoughtsAndCrosses
//
//  Created by Jeremy Pereira on 04/01/2020.
//  Copyright © 2020 Jeremy Pereira. All rights reserved.
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//
import Toolbox

/// Models a player that plays by selecting random moves weighted by past
/// success and failure.
///
/// For each move, a random square is selected but the chance of selecting
/// each square is multiplied by a weighting that is determined by success and
/// failure of using that same square in the same situation in past games.
///
/// The rules for adjusting the weightings are as follows:
/// - If the game resulted in a noughts win, all even move weightings get
///   increased by 2 and all odd move weightings get decreased by 1.
/// - If the game resulted in a cross win, all even move weightings get
///   *decreased* by 1 and all odd move weightings get *increased* by 2.
/// - If the game resulted in a draw, all move weights get increased by 1.
/// - If the game ended in an illegal move, the move weighting for the *last
///   move only* gets reduced to zero.
///
/// Note that, in the above, moves are indexed from zero, so the first move
/// by noughts is move 0 and is therefore even. The first move by crosses is
/// move 1 and is therefore odd.
///
/// Also, we don't let the weighting for any legal move drop below 1 due to a
/// flaw in our random selection that means that, if all options are zero,
/// we always take the first one.
///
/// When evaluating the game, if no result has been achieved, nothing changes.
struct StochasticPlayer: Player, Encodable
{
	typealias Weightings = Grid.Array<Double>
	/// The initial weighting for all possible moves on a given grid.
	static let startWeighting: Double = 5

	/// The current move weightings for this player
	private(set) var moves: [Grid : Weightings] = [:]

	/// Create a player
	///
	/// This llows you to set the initial move weightings for testing purposes
	/// - Parameter initialWeightings: The initial weightings - defaults to
	///                                an dictionary  with the first move set to
	///                                only allow one  corner, one edge and the middle
	init(initialWeightings: [Grid : Grid.Array<Double>] = [ Grid() : [5, 5, 0, 0, 5, 0, 0, 0, 0]])
	{
		moves = initialWeightings
	}

	mutating func chooseMove(in game: Game) -> Grid.Coordinate
	{
		let weightingsToUse = weightings(forGrid: game.grid)
		let scores = weightingsToUse.map{ Double.random(in: 0 ..< StochasticPlayer.startWeighting) * $0 }
		let scoresAndCoords = scores.enumerated().map{ (Grid.Coordinate(ordinal: $0.0), $0.1) }
		return scoresAndCoords.max{ $0.1 < $1.1 }!.0
	}

	/// Find move weightings for a particular grid
	///
	/// This is mutating because the weightings are "create on demand"
	/// - Parameter grid: The grid for which to find the weightings.
	/// - Returns: The weightings for the grid.
	mutating func weightings(forGrid grid: Grid) -> Weightings
	{
		let weightingsToUse: Weightings
		if let foundWeightings = moves[grid]
		{
			weightingsToUse = foundWeightings
		}
		else
		{
			weightingsToUse = Grid.Array(placeHolder: StochasticPlayer.startWeighting)
			moves[grid] = weightingsToUse
		}
		return weightingsToUse
	}

	mutating func evaluate(result: Game.MoveResult)
	{
		switch result
		{
		case .illegalMove(let coordinate, let game):
			var existingWeightings = weightings(forGrid: game.grid)
			existingWeightings[coordinate] = 0
			moves[game.grid] = existingWeightings
		case .win(let square, let game):
			assert(square != .empty && square == game.winner ?? .empty)
			// Get a list of all the grids and moves from there in the game
			// Zip will automatically ignore the final winning grid because
			// there is one less move.
			let gridsAndMoves = zip(game.allGrids, game.moves)
			var isNoughtMove = true
			for (grid, move) in gridsAndMoves
			{
				var existingWeightings = weightings(forGrid: grid)
				existingWeightings[move] += isNoughtMove == (square == .nought) ? 3 : -1
				if existingWeightings[move] < 1
				{
					existingWeightings[move] = 1
				}
				moves[grid] = existingWeightings
				isNoughtMove = !isNoughtMove
			}
		case .draw(let game):
			// Get a list of all the grids and moves from there in the game
			// Zip will automatically ignore the final drawn grid because
			// there is one less move.
			let gridsAndMoves = zip(game.allGrids, game.moves)
			for (grid, move) in gridsAndMoves
			{
				var existingWeightings = weightings(forGrid: grid)
				existingWeightings[move] += 1
				moves[grid] = existingWeightings
			}

		case .carryOn(_):
			break
		}
	}
}
