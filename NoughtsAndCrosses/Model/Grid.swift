//
//  Grid.swift
//  NoughtsAndCrosses
//
//  Created by Jeremy Pereira on 29/12/2019.
//  Copyright © 2019 Jeremy Pereira. All rights reserved.
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.

import Foundation

/// Models a noughts and crosses grind
struct Grid
{
	let squares: Array<Square>

	init()
	{
		squares = Array<Square>(placeHolder: .empty)
	}

	init(squares: Array<Square>)
	{
		self.squares = squares
	}


	/// Initialise from a string of `O` and `X` and spaces.
	///
	/// This is to make it easier to set up grids for testing. All charactgers
	/// other than `O`, `X` and space are ignored. So you can do things like
	/// ```
	/// O|X|O
	/// -+-+-
	/// X|O|X
	/// -+-+-
	/// O|X|O
	/// ```
	/// If the string does not contain at least 9 legal characters, it is a
	/// fatal error. Characters beyond the ninth legal character are ignored.
	///
	/// The exact set of characters that are not ignored is determined by the
	/// raw values of `Grid.Square`
	/// - Parameter string: String of `O`s, `X`s and spaces and other chars that
	///                     are ignored.
	/// - Throws: If there are not enough legal characters to make a grid
	init(string: String) throws
	{
		let squares = string.compactMap { Square(rawValue: $0) }
		guard squares.count >= 9 else { throw Grid.Error.initStringLength(string) }
		self.squares = Array(array: squares)
	}
	/// Subscript by coordinate
	/// - Parameter coordinate: The coordinate of the square to get
	/// - Returns: The square at the coordinate
	subscript(coordinate: Coordinate) -> Square
	{
		squares[coordinate]
	}

	/// Subscript by an x, y coordinate pair
	///
	/// Coordinates are from `-1 ... 1`
	/// - Parameters:
	///   - x: The x coordinate of the square to get
	///   - y: The y coordinate of the square to get
	/// - Returns: The square at the coordinate
	subscript(_ x: Int, _ y: Int) -> Square
	{
		squares[Coordinate(x, y)]
	}


	/// Return the new grid created by placing a nought or a cross in an empty square
	///
	/// - Parameters:
	///   - square: The type of square to place
	///   - position: The position to place it in
	/// - Throws: if the square to be placed is empty or the square to be
	///           replaced is not empty.
	func placing(square: Square, at position: Coordinate) throws -> Grid
	{
		guard square != .empty else { throw Error.mustPlaceNoughtOrCross }
		guard self[position] == .empty else { throw Error.squareIsNotEmpty }
		var newGrid = self.squares
		newGrid[position] = square
		return Grid(squares: newGrid)
	}

	func reversing(position: Coordinate) throws -> Grid
	{
		guard self[position] != .empty else { throw Error.squareIsEmpty }
		var newGrid = self.squares
		newGrid[position] = .empty
		return Grid(squares: newGrid)
	}
	/// If there is a winner for this grid, this is it
	var winner: Square?
	{
		if let rowWin = checkLines(lines: rows)
		{
			return rowWin
		}
		else if let colWin = checkLines(lines: columns)
		{
			return colWin
		}
		else
		{
			return checkLines(lines: diagonals)
		}
	}

	func checkLines(lines: [[Square]]) -> Square?
	{
		return lines.reduce(nil)
		{
			(resultSoFar, line) -> Square? in
			guard resultSoFar == nil else { return resultSoFar }
			if line[0] != .empty && line[0] == line[1] && line[0] == line[2]
			{
				return line[0]
			}
			else
			{
				return nil
			}
		}
	}

	var rows: [[Square]]
	{
		[
			[ squares[Coordinate(-1, -1)], squares[Coordinate(0, -1)], squares[Coordinate(1, -1)]],
			[ squares[Coordinate(-1, 0)], squares[Coordinate(0, 0)], squares[Coordinate(1, 0)]],
			[ squares[Coordinate(-1, 1)], squares[Coordinate(0, 1)], squares[Coordinate(1, 1)]],
		]
	}
	var columns: [[Square]]
	{
		[
			[ squares[Coordinate(-1, -1)], squares[Coordinate(-1, 0)], squares[Coordinate(-1, 1)]],
			[ squares[Coordinate(0, -1)], squares[Coordinate(0, 0)], squares[Coordinate(0, 1)]],
			[ squares[Coordinate(1, -1)], squares[Coordinate(1, 0)], squares[Coordinate(1, 1)]],
		]
	}

	/// The diagonal from (-1, -1) to (1, 1)
	var mainDiagonal: [Square] { [squares[Coordinate(-1, -1)], squares[Coordinate(0, 0)], squares[Coordinate(1, 1)]] }
	/// Ther diagonal from (-1, 1) to (1, -1)
	var reverseDiagonal: [Square] { [squares[Coordinate(-1, 1)], squares[Coordinate(0, 0)], squares[Coordinate(1, -1)]] }
	var diagonals: [[Square]] { [ mainDiagonal, reverseDiagonal ] }

	var emptySquares: Int
	{
		squares.filter { $0 == .empty }.count
	}
}

extension Grid
{
	enum Error: Swift.Error
	{
		case initStringLength(String)
		case gidSizeWrong([[Square]])
		case mustPlaceNoughtOrCross
		case squareIsNotEmpty
		case squareIsEmpty
	}

	enum Square: Character, CaseIterable, CustomStringConvertible
	{
		case empty = " "
		case nought = "O"
		case cross = "X"

		var description: String { String(rawValue) }
	}

	/// Coordinates are pairs of integersthat represent locations on a noughts
	/// and crosses grid
	///
	/// Each coordinate is in the range `-1 ... 1`, with (0, 0) being the
	/// centre square of the grid.
	///
	/// A special value of (`Int.max`, `Int.max`) is allowed so that a
	/// coordinates can be used to index collections (i.e. we need an `endIndex`
	/// that is not in the grid).
	struct Coordinate
	{
		let x: Int
		let y: Int

		init(_ x : Int, _ y: Int)
		{
			guard [-1, 0, 1].contains(x) && [-1, 0, 1].contains(y)
				|| (x == Int.max && y == Int.max)
				else { fatalError("x and y must both be in the range -1 ... 1") }
			self.x = x
			self.y = y
		}
	}
}

extension Grid: Hashable
{

}

extension Grid: CustomStringConvertible
{
	var description: String
	{
		let stringRows = rows.map { row in row.map { $0.description }.joined() }
		return "[" + stringRows.joined(separator: "/") + "]"
	}
}

extension Grid.Coordinate: Equatable {}

extension Grid.Coordinate: CustomStringConvertible
{
	var description: String { "(\(x), \(y))" }
}


extension Grid.Coordinate: Comparable
{
	private static let ordinals: Grid.Array<Int> = [0, 1, 2, 3, 4, 5, 6, 7, 8]
	private static let ordering =
	[
		Grid.Coordinate(-1, -1), Grid.Coordinate(0, -1), Grid.Coordinate(1, -1),
		Grid.Coordinate(-1, 0), Grid.Coordinate(0, 0), Grid.Coordinate(1, 0),
		Grid.Coordinate(-1, 1), Grid.Coordinate(0, 1), Grid.Coordinate(1, 1),
		Grid.Coordinate(Int.max, Int.max)
	]

	/// A list of all the legal coordinates of a noughts and crosses grid
	///
	/// Coordinates are guaranteed to be ordered.
	static let allCoordinates: [Grid.Coordinate] = ordering.dropLast()

	/// The ordinal number of this coordinate
	var ordinal: Int
	{
		guard self != Grid.Coordinate.endIndex else { return 9 }
		return Grid.Coordinate.ordinals[self]
	}

	static func < (lhs: Grid.Coordinate, rhs: Grid.Coordinate) -> Bool
	{
		return lhs.ordinal < rhs.ordinal
	}

	static var endIndex = Grid.Coordinate(Int.max, Int.max)

	init(ordinal: Int)
	{
		self.init(Grid.Coordinate.ordering[ordinal].x, Grid.Coordinate.ordering[ordinal].y)
	}
}

extension Grid
{
	struct Array<Element>: MutableCollection
	{

		init(placeHolder: Element)
		{
			elements = Swift.Array<Element>(repeating: placeHolder, count: 9)
		}


		/// Initialise from a Swift array.
		///
		/// Only the first 9 elements are used.
		/// - Parameter array: The array to initialise from.
		init(array: Swift.Array<Element>)
		{
			guard array.count >= 9 else { fatalError("Array must have at least 9 elements") }
			elements = Swift.Array(array[0 ..< 9])
		}

		func index(after i: Grid.Coordinate) -> Grid.Coordinate
		{
			guard i.y < 1 || i.x < 1 else { return Grid.Coordinate.endIndex }
			if i.x < 1
			{
				return Grid.Coordinate(i.x + 1, i.y)
			}
			else
			{
				return Grid.Coordinate(-1, i.y + 1)
			}
		}

		typealias Index = Grid.Coordinate
		private var elements: [Element]

		var startIndex: Grid.Coordinate { return Grid.Coordinate(-1, -1) }
		var endIndex: Grid.Coordinate { return Grid.Coordinate.endIndex }

		subscript(position: Grid.Coordinate) -> Element
		{
			get
			{
				let index = calculateInternalIndex(coordinate: position)
				return elements[index]
			}
			set
			{
				let index = calculateInternalIndex(coordinate: position)
				elements[index] = newValue
			}
		}

		private func calculateInternalIndex(coordinate: Grid.Coordinate) -> Int
		{
			guard coordinate != Grid.Coordinate.endIndex else { return 9 }
			return (coordinate.y + 1) * 3 + (coordinate.x + 1)
		}
	}
}

extension Grid.Array: Equatable where Element: Equatable
{

}

extension Grid.Array: Hashable where Element: Hashable
{

}

extension Grid.Array: ExpressibleByArrayLiteral
{
	init(arrayLiteral: Element...)
	{
		guard arrayLiteral.count == 9
			else { fatalError("literal must have nine elements") }
		self.init(array: arrayLiteral)
	}
}


extension Grid.Coordinate: Transformable
{
	static func * (lhs: Transformation, rhs: Grid.Coordinate) -> Grid.Coordinate
	{
		return lhs.transform(rhs)
	}
}


extension Grid: Transformable
{
	static func * (lhs: Transformation, rhs: Grid) -> Grid
	{
		let targetCoords = lhs * Grid.Coordinate.allCoordinates
		var targetArray = Array(placeHolder: Grid.Square.empty)
		zip(Grid.Coordinate.allCoordinates, targetCoords).forEach
		{
			let (from, to) = $0
			targetArray[to] = rhs[from]
		}
		return Grid(squares: targetArray)
	}
}

extension Grid: Encodable
{
	enum CodingKeys: String, CodingKey
	{
		case grid
	}
	
	func encode(to encoder: Encoder) throws
	{
		var container = encoder.container(keyedBy: CodingKeys.self)
		try container.encode(description, forKey: .grid)
	}
}

extension Grid.Array: Encodable where Element: Encodable
{

}
