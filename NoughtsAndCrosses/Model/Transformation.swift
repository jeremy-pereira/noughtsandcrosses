//
//  Transformation.swift
//  NoughtsAndCrosses
//
//  Created by Jeremy Pereira on 09/02/2020.
//  Copyright © 2020 Jeremy Pereira. All rights reserved.
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.

/// Represents a transformation on a grid or a coordinate or something else
///
protocol Transformation
{
	/// How the tramsformation transforms a coordinate
	/// - Parameter coordinate: The coordinate to transform
	/// - The coordinate resulting from the transform
	func transform(_ coordinate: Grid.Coordinate) -> Grid.Coordinate
	/// The inverse of this transformation
	var inverse: Transformation { get }
	/// True if this transformation leaves all coordinates unchanged.
	///
	/// A default is preovided for this which transfroms all possible coordinates
	/// and compares the result to the original. This will be a little slow,
	/// but can b e overridden
	var isIdentity: Bool { get }
}

extension Transformation
{
	var isIdentity: Bool
	{
		return self * Grid.Coordinate.allCoordinates == Grid.Coordinate.allCoordinates
	}
}


/// Models a transformation composed of two other transformations
struct ComposedTransformation: Transformation
{
	let t1: Transformation
	let t2: Transformation

	init(_ t1: Transformation, _ t2: Transformation)
	{
		self.t1 = t1
		self.t2 = t2
	}

	func transform(_ coordinate: Grid.Coordinate) -> Grid.Coordinate
	{
		return t1.transform(t2.transform(coordinate))
	}

	var inverse: Transformation
	{
		return ComposedTransformation(t2.inverse, t1.inverse)
	}

}

/// Multiply two transformations
///
/// `lhs * rhs` is the same as `{ x: Transformable in lhs(rhs(x)) }`
/// - Parameters:
///   - lhs: A transformation
///   - rhs: Another transformation
/// - Returns: the transformation that results from applying `rhs` and then
///            `lhs` to the result.
func * (lhs: Transformation, rhs: Transformation) -> Transformation
{
	return ComposedTransformation(lhs, rhs)
}
/// Models an object that can be transformed.
///
/// The object must consist of a set of components that have grid coordinates.
/// For example, a `Grid` is nine squares that each have a coordinate. So to
/// implement `Transformable` you would transform each of the coordinates of the
/// sqwuares using the transformation. Simples.
protocol Transformable
{
	/// Apply a transformation
	/// - Parameters:
	///   - lhs: The transformation to apply
	///   - rhs: The object to apply it to.
	/// - Returns: A transformed version of `self`
	static func * (lhs: Transformation, rhs: Self) -> Self
}

struct QuarterTurn: Transformation
{
	let anticlockwise: Bool

	private init(anticlockwise: Bool = false)
	{
		self.anticlockwise = anticlockwise
	}

	func transform(_ coordinate: Grid.Coordinate) -> Grid.Coordinate
	{
		return anticlockwise ? Grid.Coordinate(coordinate.y, -coordinate.x) : Grid.Coordinate(-coordinate.y, coordinate.x)
	}

	var inverse: Transformation
	{
		return QuarterTurn(anticlockwise: !anticlockwise)
	}

	static let turn: Transformation = QuarterTurn()
}

extension Array: Transformable where Element: Transformable
{
	static func * (lhs: Transformation, rhs: Array<Element>) -> Array<Element>
	{
		return rhs.map { lhs * $0 }
	}
}

/// The identity transformation
struct Identity: Transformation
{
	func transform(_ coordinate: Grid.Coordinate) -> Grid.Coordinate { coordinate }

	var inverse: Transformation { self }

	var isIdentity: Bool { true }
}
