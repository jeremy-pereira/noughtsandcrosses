//
//  MinimaxPlayer.swift
//  NoughtsAndCrosses
//
//  Created by Jeremy Pereira on 10/01/2020.
//  Copyright © 2020 Jeremy Pereira. All rights reserved.
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
import Toolbox

fileprivate let log = Logger.getLogger("NoughtsAndCrosses.MinimaxPlayer")


/// A player that implements the minimax algorithm
struct MinimaxPlayer
{
	/// The maximum depth to which the player will search
	///
	/// If you set this to 9, the whole minimax tree will be searched
	/// no matter what.
	let maxDepth: Int

	/// Function used to select one from among many move evaluations
	///
	/// Used whenever there is a list of evaluations with the same score and
	/// we need to pick one. By default, a random elelemnt is picked. You can
	/// change this function to, for example, make the player deterministic by
	/// always picking the first element.
	///
	/// - Parameter list: list of move evaluations from which one must be picked.
	/// - Returns: The picked move evbaluation
	var tieBreaker: ([MoveEvaluation]) -> (MoveEvaluation) = { $0.randomElement()! }


	init(maxDepth: Int)
	{
		self.maxDepth = maxDepth
	}

	fileprivate func minimax(game: Game, depth: Int) -> MoveEvaluation
	{
		log.debug("minimax depth = \(depth), game.grid = \(game.grid)")
		let result: MoveEvaluation
		// If the first move, we will either go top left, middle or top centre
		let possibleCoordinates: [Grid.Coordinate]

		if game.moves.count == 0
		{
			possibleCoordinates = [Grid.Coordinate(-1, -1), Grid.Coordinate(0, -1), Grid.Coordinate(0, 0),]
		}
		else if game.moves.count == 1 && game.grid[Grid.Coordinate(-1, -1)] != .empty
		{
			possibleCoordinates = [Grid.Coordinate(0, -1), Grid.Coordinate(1, -1), Grid.Coordinate(0, 0), Grid.Coordinate(1, 0), Grid.Coordinate(1, 1),]
		}
		else if game.moves.count == 1 && game.grid[Grid.Coordinate(-1, 0)] != .empty
		{
			possibleCoordinates = [Grid.Coordinate(-1, -1), Grid.Coordinate(-1, 0), Grid.Coordinate(0, 0), Grid.Coordinate(-1, 1), Grid.Coordinate(0, 1),]
		}
		else if game.moves.count == 1 && game.grid[Grid.Coordinate(0, 0)] != .empty
		{
			possibleCoordinates = [Grid.Coordinate(-1, -1), Grid.Coordinate(0, -1)]
		}
		else
		{
			possibleCoordinates = Grid.Coordinate.allCoordinates.filter{ game.grid[$0] == .empty }
		}
		let results = possibleCoordinates.map{ game.moving(at: $0) }
		let categorisedResults = Dictionary(grouping: results) {  $0.category }
		let evaluations: [MoveEvaluation]
		if let wins = categorisedResults[.win], wins.count > 0
		{
			// Add the depth to the score because a quick win is better than a long win
			evaluations = wins.map{ MoveEvaluation(move: $0.lastMove, score: $0.baseScore + depth) }
		}
		// The only way to have a draw is if we fill in the last square, so draw
		// and carry on are mutually exclusive
		else if let draw = categorisedResults[.draw]?.first
		{
			evaluations = [MoveEvaluation(move: draw.lastMove, score: draw.baseScore - depth)]
		}
		else
		{
			let carryOns = categorisedResults[.carryOn] ?? []
			// Subtract the depth because a protracted uncertain result is better
			// than a quick draw
			if depth == 0
			{
				evaluations = carryOns.map{ MoveEvaluation(move: $0.lastMove, score: -depth) }
			}
			else
			{
				let possibleEvaluations = carryOns.map{ MoveEvaluation(move: $0.lastMove, score: -minimax(game: $0.game, depth: depth - 1).score) }
				let bestScore = possibleEvaluations.max{ $0.score < $1.score }!
				evaluations = possibleEvaluations.filter{ $0.score == bestScore.score }
			}
		}
		result = tieBreaker(evaluations)
		log.debug("minimax depth = \(depth), result = \(result)")
		return result
	}

	var transformations: [UUID : Transformation] = [:]
}

extension MinimaxPlayer: Player
{
	func chooseMove(in game: Game) -> Grid.Coordinate
	{
		let transformation = findTransform(for: game)
		let transformedGame = transformation.isIdentity ? game : transformation * game
		let transformedMove = minimax(game: transformedGame, depth: maxDepth).move
		return transformation.isIdentity ? transformedMove : transformation.inverse * transformedMove
	}

	func evaluate(result: Game.MoveResult)
	{
		// Do nothing. Minimax doesn't learn from past games.
	}

	private  func findTransform(for game: Game) -> Transformation
	{
		switch game.moves.count
		{
		case 0:
			// Mimimax automatically limits itself on the first move
			return Identity()
		case 1:
			// If the centre is filled in, rotations don't matter, if a corner
			// rotate until it is in the top left. If an edge, rotate until it
			// is at the top.
			if game.grid[Grid.Coordinate(0, 0)] != .empty
				|| game.grid[Grid.Coordinate(-1, -1)] != .empty
				|| game.grid[Grid.Coordinate(0, -1)] != .empty
			{
				return Identity()
			}
			else if game.grid[Grid.Coordinate(-1, 1)] != .empty || game.grid[Grid.Coordinate(-1, 0)] != .empty
			{
				return QuarterTurn.turn
			}
			else if game.grid[Grid.Coordinate(1, 1)] != .empty || game.grid[Grid.Coordinate(0, 1)] != .empty
			{
				return QuarterTurn.turn * QuarterTurn.turn
			}
			else if game.grid[Grid.Coordinate(1, -1)] != .empty || game.grid[Grid.Coordinate(1, 0)] != .empty
			{
				return QuarterTurn.turn.inverse
			}
			else
			{
				fatalError("Can't figure out first move on \(game.grid)")
			}
		default:
			return Identity()
		}
	}
}

extension MinimaxPlayer
{
	class MoveEvaluation
	{
		init(move: Grid.Coordinate, score: Int, basedOn: MoveEvaluation? = nil)
		{
			self.move = move
			self.score = score
			self.basedOn = basedOn
		}
		let move: Grid.Coordinate
		let score: Int
		let basedOn: MoveEvaluation?
	}
}

extension MinimaxPlayer.MoveEvaluation: Comparable
{
	static func == (lhs: MinimaxPlayer.MoveEvaluation, rhs: MinimaxPlayer.MoveEvaluation) -> Bool
	{
		lhs.score == rhs.score
	}

	static func < (lhs: MinimaxPlayer.MoveEvaluation, rhs: MinimaxPlayer.MoveEvaluation) -> Bool
	{
		lhs.score < rhs.score
	}
}

fileprivate extension Game.MoveResult
{
	var baseScore: Int
	{
		switch self
		{
		case .illegalMove(_, _):
			return -10000
		case .win(_, _):
			return 100
		case .carryOn(_):
			return 0
		case .draw(_):
			return 0
		}
	}

	var lastMove: Grid.Coordinate
	{
		switch self
		{
		case .illegalMove(let coordinate, _):
			return coordinate
		default:
			return self.game.moves.last!
		}
	}

	var category: Category
	{
		switch self
		{

		case .illegalMove:
			return .illegal
		case .win(_, _):
			return .win
		case .carryOn(_):
			return .carryOn
		case .draw(_):
			return .draw
		}
	}

	enum Category: Hashable
	{
		case illegal
		case win
		case carryOn
		case draw
	}
}
