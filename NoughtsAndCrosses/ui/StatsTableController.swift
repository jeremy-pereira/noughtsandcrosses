//
//  StatsTableController.swift
//  NoughtsAndCrosses
//
//  Created by Jeremy Pereira on 23/02/2020.
//  Copyright © 2020 Jeremy Pereira. All rights reserved.
//

import Cocoa
import Toolbox

fileprivate let log = Logger.getLogger("NoughtsAndCrosses.StatsTableController")

class StatsTableController: NSObject
{
	fileprivate let noughtsNameIdentifier = NSUserInterfaceItemIdentifier("noughtsNameId")
	fileprivate let statsTable: NSTableView
	fileprivate let playerNames: [String]
	fileprivate var resultsTable = ResultsTable()
	fileprivate let noughtsColumn: NSTableColumn

	init(statsTable: NSTableView, playerNames: [String])
	{
		self.statsTable = statsTable
		self.playerNames = playerNames
		noughtsColumn = NSTableColumn(identifier: noughtsNameIdentifier)
		noughtsColumn.title = "O \\ X"
		super.init()
		playerNames.forEach
		{
			noughts in
			playerNames.forEach
			{
				crosses in
				resultsTable[noughts, crosses] = Result()
			}
		}

		setUpTableView()
	}

	func add(result: ResultRecord)
	{
		resultsTable[result.noughtsName, result.crossesName]!.add(result: result.result)
		statsTable.reloadData()
	}
}

extension StatsTableController: NSTableViewDataSource
{
	fileprivate func setUpTableView()
	{
		let tableColumns = statsTable.tableColumns
		tableColumns.forEach
		{
			statsTable.removeTableColumn($0)
		}
		statsTable.addTableColumn(noughtsColumn)
		playerNames.forEach
		{
			let column = NSTableColumn(identifier: NSUserInterfaceItemIdentifier(rawValue: $0))
			column.title = $0
			// TODO: This is a bit of a hack, should set from the cell view
			column.width = 235
			statsTable.addTableColumn(column)
		}
		statsTable.delegate = self
		statsTable.dataSource = self
		statsTable.reloadData()
	}

	func numberOfRows(in tableView: NSTableView) -> Int
	{
		return playerNames.count
	}

	func tableView(_ tableView: NSTableView, objectValueFor tableColumn: NSTableColumn?, row: Int) -> Any?
	{
		guard let identifier = tableColumn?.identifier else { return nil }
		if identifier == noughtsNameIdentifier
		{
			return playerNames[row]
		}
		else
		{
			guard let noughtsName = self.tableView(tableView, objectValueFor: noughtsColumn, row: row) as? String
				else { return nil }
			guard let crossesName = tableColumn?.identifier.rawValue else { return nil }
			return resultsTable[noughtsName, crossesName]
		}
	}

}

extension StatsTableController: NSTableViewDelegate
{
	func tableView(_ tableView: NSTableView, viewFor tableColumn: NSTableColumn?, row: Int) -> NSView?
	{
		guard let identifier = tableColumn?.identifier
		else
		{
			log.error("Failed to get the table column")
			return nil
		}
		log.debug("Making view with identifier '\(identifier)'")
		let cell: NSView
		if let existingView = statsTable.makeView(withIdentifier: identifier, owner: nil)
		{
			log.debug("Reusing cell view")
			cell = existingView
		}
		else
		{
			log.debug("Making new cell view")
			if identifier == noughtsNameIdentifier
			{
				let textCell = NSTextField()
				textCell.isEditable = false
				textCell.isSelectable = true
				cell = textCell
			}
			else
			{
				cell = ResultDetailsView()
			}
			cell.identifier = identifier
		}
		// Now populate some data. The first column seems to get populated
		// automatically.
		if let resultDetailCell = cell as? ResultDetailsView
		{
			if let resultValue = self.tableView(tableView, objectValueFor: tableColumn, row: row) as? Result
			{
				resultDetailCell.result = resultValue
			}
		}
		return cell
	}

	func tableView(_ tableView: NSTableView, heightOfRow row: Int) -> CGFloat
	{
		return 100
	}
}

extension StatsTableController
{

	/// Collects stats on who has how many  wins
	struct Result: CustomStringConvertible
	{
		/// Number of wins noughts has
		var noughts = 0
		/// Number of wins crosses has
		var crosses = 0
		/// Number of illegal moves made by **crosses** leading to a noughts win
		var noughtsIllegals = 0
		/// Number of illegal moves made by **noughts** leading to a crosses win
		var crossesIllegals = 0
		/// Number of draws
		var draws = 0
		/// Total number of games played
		var total: Int { noughts + crosses + noughtsIllegals + crossesIllegals + draws }

		var description: String { "\(noughts + noughtsIllegals) - \(crosses + crossesIllegals) / \(total)" }

		mutating func add(result: Game.MoveResult)
		{
			switch result
			{
			case .illegalMove(_, let game):
				if game.whoseGo == .nought
				{
					crossesIllegals += 1
				}
				else
				{
					noughtsIllegals += 1
				}
			case .win(let square, _):
				if square == .nought
				{
					noughts += 1
				}
				else
				{
					crosses += 1
				}
			case .carryOn(_):
				break
			case .draw:
				draws += 1
			}
		}
	}

	fileprivate struct ResultsTable
	{
		private struct Key: Hashable
		{
			let o: String
			let x: String
		}
		private var results: Dictionary<Key, Result> = [:]

		subscript(o: String, x: String) -> Result?
		{
			get { results[Key(o: o, x: x)] }
			set
			{
				results[Key(o: o, x: x)] = newValue
			}
		}
	}
}
