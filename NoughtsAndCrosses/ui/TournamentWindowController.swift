//
//  TournamentWindowController.swift
//  NoughtsAndCrosses
//
//  Created by Jeremy Pereira on 26/02/2020.
//  Copyright © 2020 Jeremy Pereira. All rights reserved.
//

import Cocoa

class TournamentWindowController: NSWindowController
{
	@IBOutlet weak var numberOfGames: NSTextField!
	@IBOutlet weak var noughtsPoolTable: NSTableView!
	@IBOutlet weak var crossesPoolTable: NSTableView!

	var playerNames: [String]?
	
    override func windowDidLoad()
	{
        super.windowDidLoad()

        // Implement this method to handle any initialization after your window
		// controller's window has been loaded from its nib file.
    }

	@IBAction func cancel(sender: Any)
	{
		tournament = nil
		self.window!.sheetParent?.endSheet(self.window!, returnCode: .cancel)
		self.window!.close()
	}

	var tournament: Tournament?

	@IBAction func go(sender: Any)
	{
		let noughtSelections = noughtsPoolTable.selectedRowIndexes
		let crossSelections = crossesPoolTable.selectedRowIndexes
		if numberOfGames.integerValue > 0
			&& noughtSelections.count > 0
			&& crossSelections.count > 0
		{
			let noughtNames = noughtSelections.map { playerNames![$0] }
			let crossNames = crossSelections.map { playerNames![$0] }

			tournament = Tournament(games: numberOfGames.integerValue, noughts: noughtNames, crosses: crossNames)
			self.window!.sheetParent?.endSheet(self.window!, returnCode: .OK)
			self.window!.close()
		}
	}
}

extension TournamentWindowController: NSTableViewDataSource
{
	func numberOfRows(in tableView: NSTableView) -> Int
	{
		return playerNames?.count ?? 0
	}

	func tableView(_ tableView: NSTableView,
				   objectValueFor tableColumn: NSTableColumn?,
				   row: Int) -> Any?
	{
		return playerNames?[row]
	}
}
