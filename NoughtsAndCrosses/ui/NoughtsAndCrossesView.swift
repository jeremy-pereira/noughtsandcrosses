//
//  NoughtsAndCrossesView.swift
//  NoughtsAndCrosses
//
//  Created by Jeremy Pereira on 24/01/2020.
//  Copyright © 2020 Jeremy Pereira. All rights reserved.
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.

import Cocoa
import Toolbox

private let log = Logger.getLogger("NoughtsAndCrosses.NoughtsAndCrossesView")

protocol GoReceiver: class
{
	func go(at coordinate: Grid.Coordinate)
}


/// A view that draws a noughts and crosses grid
///
/// The view doesn't have to be square, but it looks better if it is. It's
/// recommended to add a constraint that keeps the aspect ratio at 1:1.
///
/// The view will update itself whenever the grid is chenged.
class NoughtsAndCrossesView: NSView
{
	weak var delegate: GoReceiver?

	override var isFlipped: Bool { true }

	/// Grid this view draws
	var grid = try! Grid(string: "OXO/XOX/OXO")
	{
		didSet
		{
			DispatchQueue.main.async
			{
				self.needsDisplay = true
			}
		}
	}

	private var squareSize: NSSize { bounds.size / 3 }
	private var lineWidth: CGFloat { squareSize.width / 18 < 1 ? 1 : squareSize.width / 18 }

    override func draw(_ dirtyRect: NSRect)
	{
        super.draw(dirtyRect)

		let strokeColour = NSColor(named: NSColor.Name("gridColour")) ?? NSColor.black
		strokeColour.setStroke()
		let path = NSBezierPath()
		path.lineWidth = lineWidth
		path.move(to: NSPoint(x: squareSize.width, y: 0))
		path.line(to: NSPoint(x: squareSize.width, y: bounds.height))
		path.move(to: NSPoint(x: squareSize.width * 2, y: 0))
		path.line(to: NSPoint(x: squareSize.width * 2, y: bounds.height))
		path.move(to: NSPoint(x: 0, y: squareSize.height))
		path.line(to: NSPoint(x: bounds.width, y: squareSize.height))
		path.move(to: NSPoint(x: 0, y: squareSize.height * 2))
		path.line(to: NSPoint(x: bounds.width, y: squareSize.height * 2))
		path.stroke()

		for coordinate in Grid.Coordinate.allCoordinates
		{
			drawSquare(at: coordinate, strokeColour: strokeColour)
		}
    }

	private func drawSquare(at coordinate: Grid.Coordinate, strokeColour: NSColor)
	{
		NSGraphicsContext.saveGraphicsState()
		defer { NSGraphicsContext.restoreGraphicsState() }
		let boundingRect = nsRectForSquare(at: coordinate)
		let squareContent = grid[coordinate]
		strokeColour.setStroke()
		switch squareContent
		{
		case .cross:
			drawCross(boundingRect: boundingRect)
		case .nought:
			drawNought(boundingRect: boundingRect)
		case .empty:
			break
		}
	}

	private func nsRectForSquare(at coordinate: Grid.Coordinate) -> NSRect
	{
		let borderSize = NSSize(width: lineWidth, height: lineWidth) * 2
		let squareOrigin = NSPoint(x: CGFloat(coordinate.x  + 1) * squareSize.width,
								   y: CGFloat(coordinate.y + 1) * squareSize.height)
		let boundingRect = NSRect(origin: squareOrigin + borderSize, size: squareSize - borderSize * 2)
		return boundingRect
	}

	private func drawCross(boundingRect: NSRect)
	{
		let path = NSBezierPath()
		path.lineWidth = lineWidth
		path.move(to: boundingRect.origin)
		path.line(to: boundingRect.origin + boundingRect.size)
		path.move(to: boundingRect.origin + NSSize(width: boundingRect.width, height: 0))
		path.line(to: boundingRect.origin + NSSize(width: 0, height: boundingRect.width))
		path.stroke()
	}

	private func drawNought(boundingRect: NSRect)
	{
		let path = NSBezierPath()
		path.lineWidth = lineWidth
		path.appendOval(in: boundingRect)
		path.stroke()
	}

	override func mouseDown(with event: NSEvent)
	{
		let loc = event.locationInWindow
		let pointInView = self.convert(loc, from: nil)
		log.debug("mouse down at (window): \(loc)")
		log.debug("mouse down at (view): \(pointInView)")
		var hitSquare: Grid.Coordinate?
		for testSquare in Grid.Coordinate.allCoordinates
		{
			guard hitSquare == nil else { break }

			if nsRectForSquare(at: testSquare).contains(pointInView)
			{
				hitSquare = testSquare
			}
		}
		if let hitSquare = hitSquare, let delegate = delegate
		{
			log.debug("Going at \(hitSquare)")
			delegate.go(at: hitSquare)
		}
	}
}
