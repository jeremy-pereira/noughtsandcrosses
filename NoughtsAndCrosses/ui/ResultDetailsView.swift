//
//  ResultDetailsView.swift
//  NoughtsAndCrosses
//
//  Created by Jeremy Pereira on 01/03/2020.
//  Copyright © 2020 Jeremy Pereira. All rights reserved.
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.

import Cocoa
import Toolbox

private let log = Logger.getLogger("NoughtsAndCrosses.ResultDetailView")

/// Any object that adopts this protocol can load a nib.
///
/// This protocol is adapted from the following tutorial:
///
/// [https://www.appcoda.com/macos-custom-views/](https://www.appcoda.com/macos-custom-views/)
protocol LoadableFromNib: class
{
    var rootView: NSView? { get set }
    func load(nibName: String) -> Bool
    //func add(toParent parentView: NSView)
}

// Why do we do this this way instead of just extending NSView with the
// protocol. The reason is that we need a get/set `rootView` that becomes the
// root view required by the protocol. We can't put a property in an extension.
extension LoadableFromNib where Self: NSView
{
    func load(nibName: String) -> Bool
	{
		var nibObjects: NSArray?
		let nibName = NSNib.Name(stringLiteral: nibName)

		guard Bundle.main.loadNibNamed(nibName, owner: self, topLevelObjects: &nibObjects)
			else { return false }
		guard let allObjects = nibObjects else { return false }
		let viewObjects = allObjects.filter { $0 is NSView }

		guard let view = viewObjects.first as? NSView else { return false }
		rootView = view
		self.addSubview(view)

		view.translatesAutoresizingMaskIntoConstraints = false
		view.leadingAnchor.constraint(equalTo: self.leadingAnchor).isActive = true
		view.trailingAnchor.constraint(equalTo: self.trailingAnchor).isActive = true
		view.topAnchor.constraint(equalTo: self.topAnchor).isActive = true
		view.bottomAnchor.constraint(equalTo: self.bottomAnchor).isActive = true

		return true
	}
}

/// View to go in the results table
class ResultDetailsView: NSView, LoadableFromNib
{
	var rootView: NSView?

	@IBOutlet weak var noughtWinsField: NSTextField!
	@IBOutlet weak var crossIllegalsField: NSTextField!
	@IBOutlet weak var noughtTotalField: NSTextField!
	@IBOutlet weak var crossWinsField: NSTextField!
	@IBOutlet weak var noughtIllegalField: NSTextField!
	@IBOutlet weak var crossTotalField: NSTextField!
	@IBOutlet weak var drawsTotalField: NSTextField!
	@IBOutlet weak var totalWinsField: NSTextField!
	@IBOutlet weak var totalIllegalsField: NSTextField!
	@IBOutlet weak var totalTotalField: NSTextField!

	init()
	{
		super.init(frame: NSRect.zero)

		if !load(nibName: "ResultDetailView")
		{
			log.error("Failed to load the view.")
		}
	}

	required init?(coder: NSCoder)
	{
		super.init(coder: coder)
	}

	var result: StatsTableController.Result?
	{
		didSet
		{
			precondition(Thread.main == Thread.current)
			if let result = result
			{
				noughtWinsField.integerValue = result.noughts
				crossWinsField.integerValue = result.crosses
				noughtIllegalField.integerValue = result.noughtsIllegals
				crossIllegalsField.integerValue = result.crossesIllegals
				drawsTotalField.integerValue = result.draws
				totalTotalField.integerValue = result.total
				noughtTotalField.integerValue = result.noughts + result.noughtsIllegals
				crossTotalField.integerValue = result.crosses + result.crossesIllegals
				totalWinsField.integerValue = result.noughts + result.crosses
				totalIllegalsField.integerValue = result.noughtsIllegals + result.crossesIllegals
			}
		}
	}
}
