//
//  AppDelegate.swift
//  NoughtsAndCrosses
//
//  Created by Jeremy Pereira on 29/12/2019.
//  Copyright © 2019 Jeremy Pereira. All rights reserved.
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//

import Cocoa
import Toolbox
import Statistics

private let log = Logger.getLogger("NoughtsAndCrosses.AppDelegate")

@NSApplicationMain
class AppDelegate: NSObject, NSApplicationDelegate
{

	@IBOutlet weak var window: NSWindow!
	@IBOutlet weak var noughtSelector: NSPopUpButton!
	@IBOutlet weak var crossSelector: NSPopUpButton!
	@IBOutlet weak var gridView: NoughtsAndCrossesView!
	@IBOutlet weak var statusLabel : NSTextField!
	@IBOutlet weak var statsTable: NSTableView!

	var computerPlayer: Player = StochasticPlayer()

	private var statistics: Statistics<ResultRecord>?
	private var statsTableController: StatsTableController?

	func applicationDidFinishLaunching(_ aNotification: Notification)
	{
		knownPlayers =
		[
			HumanPlayer(name: "Me", player: self),
			ComputerPlayer(name: "Stochastic", player: StochasticPlayer()),
			ComputerPlayer(name: "Minimax", player: MinimaxPlayer(maxDepth: 9))
		]
		statusLabel.stringValue = ""
		statsTableController = StatsTableController(statsTable: statsTable,
													playerNames: knownPlayers.map{ $0.name })
	}

	func applicationWillTerminate(_ aNotification: Notification)
	{
	}

	private var knownPlayers: [PlayerDescriptor] = []
	{
		didSet
		{
			precondition(Thread.current == Thread.main)
			gameState = nil
			noughtSelector.removeAllItems()
			crossSelector.removeAllItems()
			let titles = knownPlayers.map { $0.name }
			noughtSelector.addItems(withTitles: titles)
			crossSelector.addItems(withTitles: titles)
		}
	}

	@IBAction func resetPlayer(sender: Any)
	{
		computerPlayer = StochasticPlayer()
	}

	private var gameState: GameState?
	{
		didSet
		{
			if let gameState = gameState
			{
				var needStats = true
				let statusText: String
				switch gameState.result
				{
				case .illegalMove(_, let game):
					statusText = "Illegal move by \(game.whoseGo)"
				case .win(let square, _):
					statusText = "The winner is \(square)"
				case .carryOn(let game):
					statusText = "Waiting for \(game.whoseGo)"
					needStats = false
				case .draw(_):
					statusText = "Draw"
				}
				statusLabel.stringValue = statusText
				if needStats
				{
					let resultRecord = ResultRecord(noughtsName: gameState.noughts.name,
													crossesName: gameState.crosses.name,
													result: gameState.result)
					statsTableController?.add(result: resultRecord)
					if let statistics = statistics
					{
						do
						{
							try statistics.record(dataPoint: resultRecord)
						}
						catch
						{
							log.error("\(error)")
						}
					}
					startTournamentGame()
				}
			}
		}
	}

	@IBAction func newGame(sender: Any)
	{
		let noughtsSelected: PlayerDescriptor?
		let crossesSelected: PlayerDescriptor?
		if let noughtsName = noughtSelector.selectedItem?.title
		{
			noughtsSelected = knownPlayers.first(where: { $0.name == noughtsName })
		}
		else
		{
			noughtsSelected = nil
		}
		if let crossesName = crossSelector.selectedItem?.title
		{
			crossesSelected = knownPlayers.first(where: { $0.name == crossesName })
		}
		else
		{
			crossesSelected = nil
		}

		// Can't start a game without two players.
		guard let noughts = noughtsSelected,
			  let crosses = crossesSelected
		else
		{
			return
		}
		gridView.grid = Grid()
		let gameState = GameState.new(noughts: noughts, crosses: crosses)
		self.gameState = gameState
		gameState.noughts.makeMove(controller: self, game: gameState)
	}

	fileprivate func process(state: GameState)
	{
		precondition(Thread.current == Thread.main)
		// If we started a new game
		guard state.gameNumber == gameState?.gameNumber ?? -1
		else
		{
			log.debug("Trying to process an old game \(state.gameNumber)")
			return
		}
		log.debug("processing move \(state.result)")
		gameState = state
		gridView.grid = state.result.game.grid
		switch state.result
		{
		case .carryOn(let game):
			if game.whoseGo == .nought
			{
				state.noughts.makeMove(controller: self, game: state)
			}
			else
			{
				state.crosses.makeMove(controller: self, game: state)
			}
		case .illegalMove, .win, .draw:
			state.noughts.player.evaluate(result: state.result)
			if state.crosses !== state.noughts
			{
				state.crosses.player.evaluate(result: state.result)
			}
		}
	}

	@IBAction func saveStochastic(sender: Any)
	{
		let savePanel = NSSavePanel()
		savePanel.canCreateDirectories = true
		savePanel.allowedFileTypes = ["json"]
		savePanel.beginSheetModal(for: self.window)
		{
			(response) in
			guard response == .OK else { return }
			guard let url = savePanel.url else { return }
			guard let player = self.knownPlayers.first(where: { $0.name == "Stochastic" })?.player as? StochasticPlayer
				else { return }
			do
			{
				let fm = FileManager.default
				if fm.fileExists(atPath: url.path)
				{
					try fm.removeItem(at: url)
				}
				let data = try JSONEncoder().encode(player)
				try data.write(to: url)
			}
			catch
			{
				log.error("\(error)")
				// TODO: Need a user friendly message
				self.window.presentError(error)
			}
		}

	}

	@IBAction func exportStatisticsFile(sender: Any)
	{
		let fm = FileManager.default
		guard let statistics = statistics else { return }
		let savePanel = NSSavePanel()
		savePanel.canCreateDirectories = true
		savePanel.allowedFileTypes = ["sqlite"]
		savePanel.beginSheetModal(for: self.window)
		{
			(response) in
			if response == .OK
			{
				if let url = savePanel.url
				{
					do
					{
						if fm.fileExists(atPath: url.path)
						{
							try fm.removeItem(at: url)
						}
						let sourceUrl = statistics.url
						statistics.finish()
						try fm.copyItem(at: sourceUrl, to: url)
						self.statistics = try Statistics(sqlite3File: sourceUrl.path)
					}
					catch
					{
						log.error("\(error)")
						// TODO: Need a user friendly message
						self.window.presentError(error)
					}
				}
			}
		}
	}

	private func makeStatisticsFileUrl() throws -> URL
	{
		let appSupportDirectories = FileManager.default.urls(for: .applicationSupportDirectory, in: .userDomainMask)
		// Making the assumption that we always have one.
		guard let myAppSupportDirectory = appSupportDirectories.first
		else
		{
			throw Error.cannotCreateStatsFile("Cannnot locate user's application support directory")
		}
		let myBundleIdentifier = Bundle.main.bundleIdentifier ?? "net.jeremyp.noughtsandcrosses"
		let myBundleAppSupportDirectory = myAppSupportDirectory.appendingPathComponent(myBundleIdentifier)
		try FileManager.default.createDirectory(at: myBundleAppSupportDirectory, withIntermediateDirectories: true, attributes: nil)
		let statsFile = myBundleAppSupportDirectory.appendingPathComponent("statistics.sqlite")
		return statsFile
	}

	@IBAction func recordStatistics(sender: Any)
	{
		do
		{
			let statsFileName = try makeStatisticsFileUrl()
			log.debug("stats file url '\(statsFileName)', path '\(statsFileName.path)'")
			self.statistics = try Statistics<ResultRecord>(sqlite3File: statsFileName.path)
		}
		catch
		{
			log.error("\(error)")
			// TODO: Need a user friendly message
			self.window.presentError(error)
		}
	}

	@IBAction func resetStatistics(sender: Any)
	{
		do
		{
			if let statistics = statistics
			{
				statistics.finish()
				try FileManager.default.removeItem(at: statistics.url)
				self.statistics = nil
			}
			else
			{
				let fileName = try makeStatisticsFileUrl()
				if FileManager.default.fileExists(atPath: fileName.path)
				{
					try FileManager.default.removeItem(at: fileName)
				}
			}
		}
		catch
		{
			self.window.presentError(error)
		}
	}

	var tournament: Tournament? = nil
	{
		didSet
		{
			// If we are collecting stats, we want to tag the start and end of the tournament.
			precondition(Thread.current === Thread.main, "Just be sure we are on the right thread")
			if let statistics = statistics
			{
				do
				{
					// Check if we are just starting or ending a tournament
					if tournament != nil && oldValue == nil
					{
						try statistics.tag(sequenceNumber: try statistics.latestSequenceNumber() + 1, tag: "START Tournament")
					}
					else if tournament == nil && oldValue != nil
					{
						try statistics.tag(sequenceNumber: try statistics.latestSequenceNumber(), tag: "END   Tournament")
					}
				}
				catch
				{
					self.window.presentError(error)
				}
			}
		}
	}

	var tournamentWindowController: TournamentWindowController?

	@IBAction func startTournament(sender: Any)
	{
		guard tournament == nil else { return }

		if self.tournamentWindowController == nil
		{
			self.tournamentWindowController = TournamentWindowController(windowNibName: "TournamentWindowController")
		}
		guard let tournamentWindowController = self.tournamentWindowController
			else { fatalError("Clearly we failed to create the controller") }
		tournamentWindowController.playerNames = knownPlayers.map{ $0.name }
		self.window.beginSheet(tournamentWindowController.window!)
		{
			switch $0
			{
			case .OK:
				self.tournament = tournamentWindowController.tournament
				self.startTournamentGame()
			default:
				break
			}
		}
	}

	fileprivate func startTournamentGame()
	{
		DispatchQueue.main.async
		{
			[unowned self] in

			if var tournament = self.tournament
			{
				if let (noughtsName, crossesName) = tournament.next()
				{
					self.tournament = tournament
					self.noughtSelector.selectItem(withTitle: noughtsName)
					self.crossSelector.selectItem(withTitle: crossesName)
					self.newGame(sender: self)
				}
				else
				{
					self.tournament = nil
				}
			}
		}
	}

	@objc func validateUserInterfaceItem(_ item: NSValidatedUserInterfaceItem) -> Bool
	{
		guard let action = item.action else { return false }

		switch action
		{
		case #selector(recordStatistics(sender:)):
			return statistics == nil
		case #selector(exportStatisticsFile(sender:)):
			do
			{
				let fileToExport = try makeStatisticsFileUrl()
				return FileManager.default.fileExists(atPath: fileToExport.path)
			}
			catch
			{
				log.error("Could not access internal statistics file because: '\(error)'")
				return false
			}
		default:
			return true
		}
	}
}

fileprivate extension AppDelegate
{
	// This is a class because ideally we want the player to have identity
	// semantics.
	class PlayerDescriptor
	{
		let name: String
		var player: Player

		init(name: String, player: Player)
		{
			self.name = name
			self.player = player
		}

		func makeMove(controller: AppDelegate, game: GameState)
		{
			mustBeOverridden(self)
		}
	}

	class ComputerPlayer: PlayerDescriptor
	{
		override func makeMove(controller: AppDelegate, game: GameState)
		{
			DispatchQueue.global().async
			{
				[weak self] in
				guard let strongSelf = self else { fatalError("Player has been deallocated unexpecxtedly") }
				let nextMove = strongSelf.player.chooseMove(in: game.result.game)
				let moveResult = game.result.game.moving(at: nextMove)
				DispatchQueue.main.async
				{
					controller.process(state: game.updated(result: moveResult))
				}
			}
		}
	}

	class HumanPlayer: PlayerDescriptor, GoReceiver
	{
		var controller: AppDelegate?

		override func makeMove(controller: AppDelegate, game: GameState)
		{
			guard self.controller == nil else { return }
			self.controller = controller
			controller.gridView.delegate = self
		}

		func go(at coordinate: Grid.Coordinate)
		{
			if let controller = self.controller
			{
				self.controller = nil
				controller.gridView.delegate = nil
				if let gameState = controller.gameState
				{
					let moveResult = gameState.result.game.moving(at: coordinate)
					DispatchQueue.main.async
					{
						controller.process(state: gameState.updated(result: moveResult))
					}
				}
			}
		}
	}
}

fileprivate extension AppDelegate
{
	struct GameState
	{
		private static var nextGameNumber = 1

		let gameNumber: Int
		let noughts: PlayerDescriptor
		let crosses: PlayerDescriptor
		let result: Game.MoveResult

		private init(noughts: PlayerDescriptor, crosses: PlayerDescriptor, result: Game.MoveResult? = nil, gameNumber: Int? = nil)
		{
			self.noughts = noughts
			self.crosses = crosses
			if let gameNumber = gameNumber
			{
				self.gameNumber = gameNumber
			}
			else
			{
				self.gameNumber = GameState.nextGameNumber
				GameState.nextGameNumber += 1
			}
			self.result = result ?? Game.MoveResult.carryOn(Game())
		}

		fileprivate static func new(noughts: PlayerDescriptor, crosses: PlayerDescriptor) -> GameState
		{
			return GameState(noughts: noughts, crosses: crosses)
		}

		fileprivate func updated(result: Game.MoveResult) -> GameState
		{
			return GameState(noughts: noughts, crosses: crosses, result: result, gameNumber: gameNumber)
		}
	}
}

extension AppDelegate: Player
{
	func chooseMove(in game: Game) -> Grid.Coordinate
	{
		notImplemented("The human should be choosing the move")
	}

	func evaluate(result: Game.MoveResult)
	{

	}
}

extension AppDelegate
{
	class SqlitePresenter: NSObject, NSFilePresenter
	{
		convenience init(path: String)
		{
			let url = URL(fileURLWithPath: path)
			self.init(url: url)
		}

		/// Create a presenter for the given **primary** URL
		/// - Parameter url: The url for the primary file.
		init(url: URL)
		{
			self.primaryPresentedItemURL = url
			super.init()
		}

		var primaryPresentedItemURL: URL?
		var presentedItemURL: URL?
		{
			guard let primary = primaryPresentedItemURL else { return nil }
			let primaryExtension = primary.pathExtension
			let secondaryExtension = primaryExtension + "-journal"
			return primary.deletingPathExtension().appendingPathExtension(secondaryExtension)
		}

		var presentedItemOperationQueue = OperationQueue.main

	}
}

extension AppDelegate
{
	enum Error: Swift.Error
	{
		case cannotCreateStatsFile(String)
	}
}
