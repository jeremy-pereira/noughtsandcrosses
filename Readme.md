# Noughts and Crosses

A machine that learns how to play noughts and crosses from its successes and failures.

An explanation of the mechanism may be found at <# TBD #>

## Requirements and Dependencies

The project can be built with Xcode 11.3 or later.

This project has a dependency on my *Toolbox* project which is managed by Swift Package Manager.

## State of the Union

At this time, we have a stichastic player which makes random moves until it learns from its previous games.

We also have a minimax player although it probably needs more testing.

We desperately need a user interface so a human can play games against a computer opponent.

## Blog Posts

[Noughts and Crosses](http://sincereflattery.blog/2020/01/07/noughts-and-crosses/)
