//
//  MinimaxTests.swift
//  NoughtsAndCrossesTests
//
//  Created by Jeremy Pereira on 11/01/2020.
//  Copyright © 2020 Jeremy Pereira. All rights reserved.
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.

import XCTest
import Toolbox
@testable import NoughtsAndCrosses

fileprivate class AlwaysZeroRng: RandomNumberGenerator
{
	func next() -> UInt64
	{
		return 0
	}
}

class MinimaxTests: XCTestCase
{
	func testRNG()
	{
		var rng = AlwaysZeroRng()

		let a = [0, 1]
		let n = a.randomElement(using: &rng)
		XCTAssert(n == 0)
	}

	func testWrappedRng()
	{
		var myRng = AnyRNG.wrap(AlwaysZeroRng())
		let a = [0, 1]
		let n = a.randomElement(using: &myRng)
		XCTAssert(n == 0)
	}

	func testImmediateWin()
	{
		let start = Game()
		let next = Game.MoveResult.unit(start)
			>>- { $0.moving(at: Grid.Coordinate(0, 0)) }	// [   / O /   ]
			>>- { $0.moving(at: Grid.Coordinate(0, -1)) }	// [ X / O /   ]
			>>- { $0.moving(at: Grid.Coordinate(-1, -1)) }	// [OX / O /   ]
			>>- { $0.moving(at: Grid.Coordinate(1, 0)) }	// [OX / OX/   ]

		guard case Game.MoveResult.carryOn(let game) = next
			else { fatalError("Last move should have been a .carryOn") }

		var player = MinimaxPlayer(maxDepth: 9)

		let coordinate = player.chooseMove(in: game)
		XCTAssert(coordinate == Grid.Coordinate(1, 1))
	}

	func testImmediateLoss()
	{
		let start = Game()
		let next = Game.MoveResult.unit(start)
			>>- { $0.moving(at: Grid.Coordinate(0, 0)) }	// [   / O /   ]
			>>- { $0.moving(at: Grid.Coordinate(0, -1)) }	// [ X / O /   ]
			>>- { $0.moving(at: Grid.Coordinate(-1, -1)) }	// [OX / O /   ]

		guard case Game.MoveResult.carryOn(let game) = next
			else { fatalError("Last move should have been a .carryOn") }

		var player = MinimaxPlayer(maxDepth: 9)

		let coordinate = player.chooseMove(in: game)
		XCTAssert(coordinate == Grid.Coordinate(1, 1))

	}

	fileprivate let alwaysZeroFirstMove = Grid.Coordinate(-1, -1)

	func testFirstMove()
	{
		let start = Game()
		var player = MinimaxPlayer(maxDepth: 9)
		player.tieBreaker = { $0.first! }

		let coordinate = player.chooseMove(in: start)
		XCTAssert(coordinate == alwaysZeroFirstMove, "Selected coordinate: \(coordinate)")
	}

	// First run 55s
	// Separate out rows - 53s
	// rows got differently - 46s
	// rows, cols, diagonals hard coded = 36s
	// optimisation for 5 moves = 35s
	// Optimise for game check = 35s
	// Filter out non empty squares = 16s
	// Limit to three squares = 4s
	// Eliminated random shuffle = 3.2s
    func testPerformance()
    {
		var count = 0
        // This is an example of a performance test case.
        self.measure
		{
			count += 1
			let start = Game()
			var player = MinimaxPlayer(maxDepth: 9)
			player.tieBreaker = { $0.first! }

			let coordinate = player.chooseMove(in: start)
			print("Finished round \(count)")
			XCTAssert(coordinate == alwaysZeroFirstMove, "It seems the attempted deterministic selection does not work")
        }
    }

}
