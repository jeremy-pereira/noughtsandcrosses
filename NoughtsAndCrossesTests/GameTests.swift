//
//  GameTests.swift
//  NoughtsAndCrossesTests
//
//  Created by Jeremy Pereira on 04/01/2020.
//  Copyright © 2020 Jeremy Pereira. All rights reserved.
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//

import XCTest
import Toolbox
@testable import NoughtsAndCrosses

class GameTests: XCTestCase
{

    func testMove()
	{
		let start = Game()
		let next = Game.MoveResult.unit(start) >>- { $0.moving(at: Grid.Coordinate(0, 0)) }
		switch next
		{
		case .illegalMove(_, _):
			XCTFail("Got illegal move")
		case .win(_, _):
			XCTFail("Got a win after one move")
		case .carryOn(let game):
			XCTAssert(try! game.grid == Grid(string: "   / O /   "))
		case .draw(_):
			XCTFail("Got a draw after one move")
		}
    }

	func testWinSequence()
	{
		let start = Game()
		let next = Game.MoveResult.unit(start)
			>>- { $0.moving(at: Grid.Coordinate(0, 0)) }
			>>- { $0.moving(at: Grid.Coordinate(0, -1)) }
			>>- { $0.moving(at: Grid.Coordinate(-1, -1)) }
			>>- { $0.moving(at: Grid.Coordinate(1, 0)) }
			>>- { $0.moving(at: Grid.Coordinate(1, 1)) } // The winning move
			>>- { $0.moving(at: Grid.Coordinate(0, 1)) }
		switch next
		{
		case .illegalMove(let coordinate, let game):
			XCTFail("Got illegal move: \(coordinate) in  \(game.grid)")
		case .win(let square, let game):
			XCTAssert(square == .nought)
			XCTAssert(try! game.grid == Grid(string: "[OX / OX/  O]"), "The grid was wrong: \(game.grid)")
		case .carryOn(let game):
			XCTFail("Should already have won \(game.grid)")
		case .draw(let game):
			XCTFail("Got a draw \(game.grid)")
		}
	}

	func testDrawSequence()
	{
		let start = Game()
		let next = Game.MoveResult.unit(start)
			>>- { $0.moving(at: Grid.Coordinate(0, 0)) } 	// [   / O /   ]
			>>- { $0.moving(at: Grid.Coordinate(-1, -1)) }	// [X  / O /   ]
			>>- { $0.moving(at: Grid.Coordinate(0, -1)) }	// [XO / O /   ]
			>>- { $0.moving(at: Grid.Coordinate(0, 1)) }	// [XO / O / X ]
			>>- { $0.moving(at: Grid.Coordinate(1, -1)) }	// [XOO/ O / X ]
			>>- { $0.moving(at: Grid.Coordinate(-1, 1)) }	// [XOO/ O /XX ]
			>>- { $0.moving(at: Grid.Coordinate(1, 1)) }	// [XOO/ O /XXO]
			>>- { $0.moving(at: Grid.Coordinate(1, 0)) }	// [XOO/ OX/XXO]
			>>- { $0.moving(at: Grid.Coordinate(-1, 0)) }	// [XOO/OOX/XXO] (draw)
			>>- { $0.moving(at: Grid.Coordinate(-1, 0)) }	// [XOO/OOX/XXO]
		switch next
		{
		case .illegalMove(let coordinate, let game):
			XCTFail("Got illegal move: \(coordinate) in  \(game.grid)")
		case .win(_, let game):
			XCTFail("Got a win \(game.grid)")
			XCTAssert(try! game.grid == Grid(string: "[OX / OX/  O]"), "The grid was wrong: \(game.grid)")
		case .carryOn(let game):
			XCTFail("Should already have won \(game.grid)")
		case .draw(let game):
			XCTAssert(try! game.grid == Grid(string: "[XOO/OOX/XXO]"), "The grid was wrong: \(game.grid)")
		}
	}

	func testIllegalMoveSequence()
	{
		let start = Game()
		let next = Game.MoveResult.unit(start)
			>>- { $0.moving(at: Grid.Coordinate(0, 0)) }
			>>- { $0.moving(at: Grid.Coordinate(0, 0)) } // Illegal
			>>- { $0.moving(at: Grid.Coordinate(-1, -1)) }
			>>- { $0.moving(at: Grid.Coordinate(1, 0)) }
			>>- { $0.moving(at: Grid.Coordinate(1, 1)) }
			>>- { $0.moving(at: Grid.Coordinate(0, 1)) }
		switch next
		{
		case .illegalMove(let coordinate, let game):
			XCTAssert(coordinate == Grid.Coordinate(0, 0))
			XCTAssert(try! game.grid == Grid(string: "[   / O /   ]"))
		case .win(_, let game):
			XCTFail("Got a win: \(game.grid)")
		case .carryOn(let game):
			XCTFail("Should already have won \(game.grid)")
		case .draw(let game):
			XCTFail("Got a draw: \(game.grid)")
		}
	}
}
