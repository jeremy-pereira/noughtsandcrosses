//
//  TransformationTests.swift
//  NoughtsAndCrossesTests
//
//  Created by Jeremy Pereira on 10/02/2020.
//  Copyright © 2020 Jeremy Pereira. All rights reserved.
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.

import XCTest
@testable import NoughtsAndCrosses
import Toolbox

class TransformationTests: XCTestCase
{

    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

	let quarterTurnCoords =
	[
		Grid.Coordinate(1, -1), Grid.Coordinate(1, 0), Grid.Coordinate(1, 1),
		Grid.Coordinate(0, -1), Grid.Coordinate(0, 0), Grid.Coordinate(0, 1),
		Grid.Coordinate(-1, -1), Grid.Coordinate(-1, 0), Grid.Coordinate(-1, 1),
	]

    func testQuarterTurn()
	{
		let results = Grid.Coordinate.allCoordinates.map{ QuarterTurn.turn * $0 }
		print("All coordinates: \(Grid.Coordinate.allCoordinates)")
		XCTAssert(results == quarterTurnCoords, "Wrong results, got \(results)")
    }

	func testInverse()
	{
		let results = quarterTurnCoords.map{ QuarterTurn.turn.inverse * $0 }
		XCTAssert(results == Grid.Coordinate.allCoordinates, "Wrong results, got \(results)")

	}

	func testComposition()
	{
		let composed = QuarterTurn.turn * QuarterTurn.turn.inverse

		let results = Grid.Coordinate.allCoordinates.map{ composed * $0 }
		XCTAssert(results == Grid.Coordinate.allCoordinates)
	}

	func testComposedInverse()
	{
		let composed = QuarterTurn.turn * QuarterTurn.turn * QuarterTurn.turn
		let intermediate = quarterTurnCoords.map { composed * $0 }
		let results = intermediate.map { composed.inverse * $0 }
		XCTAssert(results == quarterTurnCoords)
	}

	func testArrayExtension()
	{
		let mapResult = quarterTurnCoords.map { QuarterTurn.turn * $0 }
		let extensionResult = QuarterTurn.turn * quarterTurnCoords
		XCTAssert(extensionResult == mapResult)
	}

	func testGrid()
	{
		let startGrid = try! Grid(string: "[OOX/OOX/OOO]")
		let targetGrid = try! Grid(string: "[OOO/OOO/OXX]")
		XCTAssert(QuarterTurn.turn * startGrid == targetGrid)
	}

	func testGame()
	{
		let start = Game()
		let next = Game.MoveResult.unit(start)
			>>- { $0.moving(at: Grid.Coordinate(0, 0)) } 	// [   / O /   ]
			>>- { $0.moving(at: Grid.Coordinate(-1, -1)) }	// [X  / O /   ]
			>>- { $0.moving(at: Grid.Coordinate(0, -1)) }	// [XO / O /   ]
			>>- { $0.moving(at: Grid.Coordinate(0, 1)) }	// [XO / O / X ]
			>>- { $0.moving(at: Grid.Coordinate(1, -1)) }	// [XOO/ O / X ]
			>>- { $0.moving(at: Grid.Coordinate(-1, 1)) }	// [XOO/ O /XX ]
			>>- { $0.moving(at: Grid.Coordinate(1, 1)) }	// [XOO/ O /XXO]
			>>- { $0.moving(at: Grid.Coordinate(1, 0)) }	// [XOO/ OX/XXO]
			>>- { $0.moving(at: Grid.Coordinate(-1, 0)) }	// [XOO/OOX/XXO] (draw)
		let gameToTest = next.game
		let rotatedGame = QuarterTurn.turn * gameToTest
		let targetGrid = try! Grid(string: "[XOX/XOO/OXO]")
		XCTAssert(targetGrid == rotatedGame.grid)
		XCTAssert(rotatedGame.moves ==
			[
				Grid.Coordinate(0, 0),
				Grid.Coordinate(1, -1),
				Grid.Coordinate(1, 0),
				Grid.Coordinate(-1, 0),
				Grid.Coordinate(1, 1),
				Grid.Coordinate(-1, -1),
				Grid.Coordinate(-1, 1),
				Grid.Coordinate(0, 1),
				Grid.Coordinate(0, -1),
			])
	}
}
