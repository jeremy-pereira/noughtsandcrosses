//
//  StatisticsTests.swift
//  NoughtsAndCrossesTests
//
//  Created by Jeremy Pereira on 15/02/2020.
//  Copyright © 2020 Jeremy Pereira. All rights reserved.
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.

import XCTest
import Foundation
import Toolbox
import SQL
import Statistics
@testable import NoughtsAndCrosses

private let log = Logger.getLogger("NoughtsAndCrossesTests.StatisticsTests")

fileprivate struct TestStats: Recordable
{
	var data: [String : SQLValue]
	{
		[
			"count" : .integer(count),
			"total" : .integer(total),
			"average" : .float(average)
		]
	}

	static var dataSetName = "foo"

	static var columns: [(String, SQLType)] =
	[
		("count", .integer),
		("total", .integer),
		("average", .float),
	]

	var count: Int
	var total: Int
	var average: Double
}

fileprivate struct PivotData: Recordable
{
	static let dataSetName = "pivot"

	static let columns: [(String, SQLType)] =
	[
		("rowName", .text),
		("colName", .text),
		("value", .integer)
	]

	init(row: String, col: String, value: Int)
	{
		data = ["rowName" : SQLValue.char(row),
				"colName": SQLValue.char(col),
				"value" : SQLValue.integer(value)]
	}

	var data: [String : SQLValue]
}

class StatisticsTests: XCTestCase
{
	let createFromScratchDb = "createFromScratch.sqlite3"
	let resultsDb = "results.sqlite3"
	let fm = FileManager.default

    override func setUp()
	{
		log.pushLevel(.info)
		log.info("Where am I? \(fm.currentDirectoryPath)")
		// Delete the DB that is not supposed to exist
		deleteFile(name: createFromScratchDb)
		deleteFile(name: resultsDb)
    }

	private func deleteFile(name: String)
	{
        var dbBytes = try! name.signedBytes(encoding: String.Encoding.utf8)
        dbBytes.append(0)
        unlink(&dbBytes)
	}

    override func tearDown()
	{
		log.popLevel()
    }

    func testRecordMoveResult()
	{
		do
		{
			let stats = try Statistics<ResultRecord>(sqlite3File: resultsDb)
			log.info("Created \(fm.currentDirectoryPath)/\(resultsDb)")
			let dataPoint = ResultRecord(noughtsName: "noughts", crossesName: "crosses", result: Game.MoveResult.draw(Game()))
			try stats.record(dataPoint: dataPoint)
			try stats.record(dataPoint: dataPoint)
			let countSelect = Statement.select(columnNames: ["count(*)"], from: TableName(ResultRecord.dataSetName))
			try stats.connection!.with(statement: countSelect)
			{
				try $0.execute()
				let results = $0.resultSet
				guard let firstResult = try results.next()
					else { XCTFail("No count result ") ; return }
				guard let count = firstResult["count(*)"]
					else { XCTFail("count not called count") ; return }
				XCTAssert(count.asInt == 2, "Wrong count \(count)")
			}
		}
		catch
		{
			XCTFail("\(error)")
		}
    }
}
