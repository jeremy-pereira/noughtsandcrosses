//
//  NoughtsAndCrossesTests.swift
//  NoughtsAndCrossesTests
//
//  Created by Jeremy Pereira on 29/12/2019.
//  Copyright © 2019 Jeremy Pereira. All rights reserved.
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//

import XCTest
@testable import NoughtsAndCrosses

class NoughtsAndCrossesTests: XCTestCase
{

    override func setUp()
	{
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown()
	{
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testInitGridString()
	{
		let gridString1 = """
			OXO
			XOX
			OXO
			"""
		let grid = try! Grid(string: gridString1)
		let coordinate = Grid.Coordinate(1, 0)
		XCTAssert(grid[coordinate] == .cross)
    }

	func testEmptyGrid()
	{
		let grid = Grid()
		for x in -1 ... 1
		{
			for y in -1 ... 1
			{
				XCTAssert(grid[x, y] == .empty)
			}
		}
	}

	func testHashableGrid()
	{
		let gridString1 = """
			OXO
			X X
			OXO
			"""
		let gridString2 = """
			O|X|O
			-+-+-
			X| |X
			-+-+-
			O|X|O
			"""
		let gridString3 = """
			O|X|O
			-+-+-
			X| |X
			-+-+-
			O|X| .
			"""
		let grid1 = try! Grid(string: gridString1)
		let grid2 = try! Grid(string: gridString2)
		let grid3 = try! Grid(string: gridString3)
		XCTAssert(grid1 == grid2)
		XCTAssert(grid1.hashValue == grid2.hashValue)
		XCTAssert(grid1 != grid3)
	}

	func testPlacement()
	{
		let gridString1 = """
			OXO
			X X
			OXO
			"""
		let gridString2 = """
			OXO
			XOX
			OXO
			"""
		let grid1 = try! Grid(string: gridString1)
		let grid2 = try! Grid(string: gridString2)
		do
		{
			let movedGrid = try grid1.placing(square: .nought, at: Grid.Coordinate(0, 0))
			XCTAssert(movedGrid == grid2)
		}
		catch
		{
			XCTFail("Failed to move to empty square: \(error)")
		}
		// Make sure we can't place an empty square
		do
		{
			_ = try grid1.placing(square: .empty, at: Grid.Coordinate(0, 0))
			XCTFail("Should not be able to place an empty square")
		}
		catch Grid.Error.mustPlaceNoughtOrCross {}
		catch
		{
			XCTFail("Error occurred: \(error)")
		}
		// Make sure we can't place in an empty square
		do
		{
			_ = try grid1.placing(square: .nought, at: Grid.Coordinate(1, 0))
			XCTFail("Should not be able to place an empty square")
		}
		catch Grid.Error.squareIsNotEmpty {}
		catch
		{
			XCTFail("Error occurred: \(error)")
		}
	}

	func testWinnerDetection()
	{
		detectWinner(string: "[OXO/X X/OXO]", expected: nil)
		detectWinner(string: "[OXO/XOX/OXO]", expected: .nought)
		detectWinner(string: "[OOO/   /OXO]", expected: .nought)
		detectWinner(string: "[O O/XXX/O O]", expected: .cross)
		detectWinner(string: "[O O/   /XXX]", expected: .cross)
		detectWinner(string: "[O  /O  /OXX]", expected: .nought)
		detectWinner(string: "[ O / O /OOX]", expected: .nought)
		detectWinner(string: "[  O/  O/OXO]", expected: .nought)
		detectWinner(string: "[XXO/XOX/OXX]", expected: .nought)
	}

	private func detectWinner(string: String, expected: Grid.Square?)
	{
		let grid = try! Grid(string: string)
		let expectedDesc = expected?.description ?? "none"
		XCTAssert(grid.winner == expected, "Grid \(grid) should have winner \(expectedDesc)")
	}

	func testGridDescription()
	{
		[
			"[OXO/X X/OXO]",
			"[OXO/X X/OX ]",
		].forEach
		{
			let desc = try! Grid(string: $0).description
			XCTAssert($0 == desc, "Got description wrong expected \($0), got \(desc)")
		}
	}

	func testRows()
	{
		let grid = try! Grid(string: "[O  / O /  O]")
		let rows = grid.rows
		XCTAssert(rows[0] == [.nought, .empty, .empty], "Got \(rows[0])")
		XCTAssert(rows[1] == [.empty, .nought, .empty], "Got \(rows[1])")
		XCTAssert(rows[2] == [.empty, .empty, .nought], "Got \(rows[2])")
	}
}
