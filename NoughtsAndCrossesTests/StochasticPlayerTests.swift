//
//  StochasticPlayerTests.swift
//  NoughtsAndCrossesTests
//
//  Created by Jeremy Pereira on 04/01/2020.
//  Copyright © 2020 Jeremy Pereira. All rights reserved.
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//

import XCTest
import Toolbox
@testable import NoughtsAndCrosses

private let log = Logger.getLogger("NoughtsAndCrossesTests.StochasticPlayerTests")

class StochasticPlayerTests: XCTestCase
{

    override func setUp()
	{
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown()
	{
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testFirstMove()
	{
		let weightings = Grid.Array<Double>(array: [0, 0, 0, 0, 10, 0, 0, 0, 0])
		let moveWeightings = [Grid() : weightings]
		var player = StochasticPlayer(initialWeightings: moveWeightings)
		let game = Game()
		let move = player.chooseMove(in: game)
		XCTAssert(move == Grid.Coordinate(0, 0))
    }

	func testAddMove()
	{
		var player = StochasticPlayer()
		XCTAssert(player.moves.count == 0)
		let game = Game()
		let move = player.chooseMove(in: game)
		print("Move chosen was \(move)")
		XCTAssert(player.moves.count == 1)

	}

	func testIllegalMoveEvalution()
	{
		var player = StochasticPlayer()
		let start = Game()
		let next = Game.MoveResult.unit(start)
			>>- { $0.moving(at: Grid.Coordinate(0, 0)) }
			>>- { $0.moving(at: Grid.Coordinate(0, 0)) } // Illegal
			>>- { $0.moving(at: Grid.Coordinate(-1, -1)) }
			>>- { $0.moving(at: Grid.Coordinate(1, 0)) }
			>>- { $0.moving(at: Grid.Coordinate(1, 1)) }
			>>- { $0.moving(at: Grid.Coordinate(0, 1)) }
		player.evaluate(result: next)
		switch next
		{
		case .illegalMove(let coordinate, let game):
			XCTAssert(coordinate == Grid.Coordinate(0, 0))
			XCTAssert(try! game.grid == Grid(string: "[   / O /   ]"))
			let weightings = player.weightings(forGrid: game.grid)
			XCTAssert(weightings[coordinate] == 0, "Weighting for illegal move should be zero, not \(weightings[coordinate])")

		default:
			fatalError("Move sequence should have resulted in an illegal move")
		}
	}

	func testWinEvaluation()
	{
		let start = Game()
		let next = Game.MoveResult.unit(start)
			>>- { $0.moving(at: Grid.Coordinate(0, 0)) }	// [   / O /   ]
			>>- { $0.moving(at: Grid.Coordinate(0, -1)) }	// [ X / O /   ]
			>>- { $0.moving(at: Grid.Coordinate(-1, -1)) }	// [OX / O /   ]
			>>- { $0.moving(at: Grid.Coordinate(1, 0)) }	// [OX / OX/   ]
			>>- { $0.moving(at: Grid.Coordinate(1, 1)) }	// [OX / OX/  O]

		var player = StochasticPlayer()
		player.evaluate(result: next)
		switch next
		{
		case .win(_, _):
			var grid = Grid()
			XCTAssert(player.weightings(forGrid: grid)[Grid.Coordinate(0, 0)] == 8)
			XCTAssert(player.weightings(forGrid: grid)[Grid.Coordinate(0, -1)] == 5)
			grid = try! grid.placing(square: .nought, at: Grid.Coordinate(0, 0))
			XCTAssert(player.weightings(forGrid: grid)[Grid.Coordinate(0, -1)] == 4, "Got \(player.weightings(forGrid: grid)) for \(grid)")
			XCTAssert(player.weightings(forGrid: grid)[Grid.Coordinate(0, 0)] == 5)
		default:
			fatalError("Move sequence should have resulted in a win for noughts")
		}
	}

	func testDrawEvaluation()
	{
		let start = Game()
		let next = Game.MoveResult.unit(start)
			>>- { $0.moving(at: Grid.Coordinate(0, 0)) } 	// [   / O /   ]
			>>- { $0.moving(at: Grid.Coordinate(-1, -1)) }	// [X  / O /   ]
			>>- { $0.moving(at: Grid.Coordinate(0, -1)) }	// [XO / O /   ]
			>>- { $0.moving(at: Grid.Coordinate(0, 1)) }	// [XO / O / X ]
			>>- { $0.moving(at: Grid.Coordinate(1, -1)) }	// [XOO/ O / X ]
			>>- { $0.moving(at: Grid.Coordinate(-1, 1)) }	// [XOO/ O /XX ]
			>>- { $0.moving(at: Grid.Coordinate(1, 1)) }	// [XOO/ O /XXO]
			>>- { $0.moving(at: Grid.Coordinate(1, 0)) }	// [XOO/ OX/XXO]
			>>- { $0.moving(at: Grid.Coordinate(-1, 0)) }	// [XOO/OOX/XXO] (draw)
		var player = StochasticPlayer()
		player.evaluate(result: next)
		switch next
		{
		case .draw(_):
			var grid = Grid()
			XCTAssert(player.weightings(forGrid: grid)[Grid.Coordinate(0, 0)] == 6, "Got \(player.weightings(forGrid: grid)) for \(grid)")
			XCTAssert(player.weightings(forGrid: grid)[Grid.Coordinate(0, -1)] == 5)
			grid = try! grid.placing(square: .nought, at: Grid.Coordinate(0, 0))
			XCTAssert(player.weightings(forGrid: grid)[Grid.Coordinate(-1, -1)] == 6, "Got \(player.weightings(forGrid: grid)) for \(grid)")
			XCTAssert(player.weightings(forGrid: grid)[Grid.Coordinate(0, 0)] == 5)
		default:
			fatalError("Move sequence should have resulted in a draw")
		}
	}

	func testRunMultipleGames()
	{
		var player = StochasticPlayer()
		print("O\tX\tdraw\tillegal\tpositions")
		for _ in 0 ..< 100
		{
			let (noughtWins, crossWins, draws, illegalMoves, positionsKnown) = playGames(player: &player, count: 40)
			print("\(noughtWins)\t\(crossWins)\t\(draws)\t\(illegalMoves)\t\(positionsKnown)")
		}
		let encoder = JSONEncoder()
		encoder.outputFormatting = .prettyPrinted

		do
		{
			let data = try encoder.encode(player)
			log.debug(String(data: data, encoding: .utf8)!)
		}
		catch
		{
			XCTFail("\(error)")
		}
	}

	private func playGames(player: inout StochasticPlayer, count: Int) -> (noughtWins: Int, crossWins: Int, draws: Int, illegalMoves: Int, positionsKnown: Int)
	{
		var noughtWins = 0
		var crossWins = 0
		var draws = 0
		var illegalMoves = 0
		for _ in 0 ..< count
		{
			var result: Game.MoveResult = .carryOn(Game())
			while case Game.MoveResult.carryOn(let game) = result
			{
				let move = player.chooseMove(in: game)
				result = game.moving(at: move)
			}
			switch result
			{
			case .illegalMove(_, _):
				illegalMoves += 1
			case .win(let square, _):
				if square == .cross
				{
					crossWins += 1
				}
				else
				{
					noughtWins += 1
				}
			case .carryOn(_):
				fatalError("Should not be able to exit the loop without completing the game")
			case .draw(_):
				draws += 1
			}
			player.evaluate(result: result)
		}
		return (noughtWins, crossWins, draws, illegalMoves, player.moves.count)
	}

	// Baseline before trying transforms is 0.303s
    func testPerformance()
    {
		var player = StochasticPlayer()
		_ = playGames(player: &player, count: 1000) // Try to stabilise the known moves a  bit

		var count = 0
        // This is an example of a performance test case.
        self.measure
		{
			count += 1
			_ = playGames(player: &player, count: 300)
			print("Finished round \(count)")
        }
    }

}
